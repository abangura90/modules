resource "aws_instance" "myec2" {
    ami = "ami-00068cd7555f543d5"
    //instance_type = "t2.micro"
    instance_type = var.instance_type
    //instance_type = "${lookup(var.instance_type, terraform.workspace)}"
    security_groups = ["default"]

    tags = {
      Name = "myec2"
    }

}